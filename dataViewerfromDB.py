import os
import sys

import matplotlib
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyodbc
from matplotlib.widgets import Slider
from mpl_toolkits.mplot3d import Axes3D

ROOT_DIR = os.path.expanduser('~')
desktop_path = ROOT_DIR + "/Desktop"
viewer_path = f'{desktop_path}/prevla/prevla_viewer/gifs'


exp = int(sys.argv[1])  # Take exp no via terminal argument
# rep = int(sys.argv[2])

# Connection with database
cnxn = pyodbc.connect(
    'DRIVER={ODBC Driver 18 for SQL Server};SERVER=193.35.200.106;DATABASE=Prevla;UID=SA;PWD=hP337^9nArG&;TrustServerCertificate=Yes')

# plt.get_backend()
# matplotlib.use("QTAgg")

print("Data Viewer from DB has executed")


################################################
# Taking log from database
cursor = cnxn.cursor()

log_sql_query = "SELECT * FROM Data_Logs WHERE deney_no = ? ;"

log_df = pd.read_sql(log_sql_query, cnxn, params=[exp])

if (log_df.empty):
    print("Experiment not found")
    exit(1)

# Save start and end timestamp of experiment
startTime = float(log_df.loc[0, "startTimestamp_current_part"])
endTime = float(log_df.loc[log_df.index[-1], "endTimestamp_current_part"])
duration = endTime-startTime
params = (startTime, endTime)

print("starTime:", startTime)
print("endTime:", endTime)

################################################

# Taking mediapipe data from database
mediapipe_sql_query = "SELECT * FROM MediapipeDatas WHERE mp_1_p1_mp_h_p_timestamp BETWEEN ? AND ? ORDER BY mp_Id;"
mediaDF = pd.read_sql(mediapipe_sql_query, cnxn, params=params)

if (not mediaDF.empty):
    # Mediapipe frame ratio
    # print(mediaDF)
    mediapipeRatio = (30*duration) / len(mediaDF)

    # Mediapipe x,y,z points
    x_points = mediaDF[mediaDF.columns[mediaDF.columns.str.contains(
        'x_point')]].to_numpy()
    y_points = mediaDF[mediaDF.columns[mediaDF.columns.str.contains(
        'y_point')]].to_numpy()
    z_points = mediaDF[mediaDF.columns[mediaDF.columns.str.contains(
        'z_point')]].to_numpy()

    mediaTimeStamps = mediaDF["mp_1_p1_mp_h_p_timestamp"].to_numpy()


# Taking radar data from database and check if empty or not
radar_sql_query = "SELECT * FROM RadarDatas WHERE rdr_timestamp BETWEEN ? AND ? ORDER BY radar_Id;"
radar_data = pd.read_sql(radar_sql_query, cnxn, params=params)

if (not radar_data.empty):
    radarRatio = 0
    radarRatio = (30*duration)/len(range(
        radar_data.loc[0, "rdr_frame_no"], radar_data.loc[radar_data.index[-1], "rdr_frame_no"]))
    # First frame of radar data
    firstFrame = radar_data.loc[0, "rdr_frame_no"]


# Taking kinect data from database and check if empty or not
kinect_sql_query = "SELECT * FROM KinectDatas WHERE kinect_timestamp BETWEEN ? AND ? ORDER BY kinect_id;"
kinect_data = pd.read_sql(kinect_sql_query, cnxn, params=params)

if (not kinect_data.empty):
    kinectRatio = 0
    # Kinect x,y,z points
    kinect_x_points = kinect_data[kinect_data.columns[kinect_data.columns.str.contains(
        'x_pos')]].to_numpy()
    kinect_y_points = kinect_data[kinect_data.columns[kinect_data.columns.str.contains(
        'y_pos')]].to_numpy()
    kinect_z_points = kinect_data[kinect_data.columns[kinect_data.columns.str.contains(
        'z_pos')]].to_numpy()
    kinect_timestamp = kinect_data["kinect_timestamp"].to_numpy()

    # Kinect point normalisation
    kinect_x_points = (kinect_x_points + 1)/2
    kinect_y_points = (kinect_y_points + 1)/2

    # TODO
    # kinect_z_points = (kinect_z_points + 1)/2

    # Kinect Frame Ratio
    kinectRatio = (30*duration)/len(kinect_data)

# plt settings
fig = plt.figure(figsize=(10, 5))
# slider_ax = fig.add_subplot(111)
# slider = Slider(ax=slider_ax,label = 'frame',valmin=0,valmax=100,valinit=30)
radar_fig = fig.add_subplot(131, projection='3d')
media_fig = fig.add_subplot(132, projection='3d')
kinect_fig = fig.add_subplot(133, projection='3d')

radar_scatter = radar_fig.scatter([], [], [], c='darkblue', alpha=0.5)
media_scatter = media_fig.scatter([], [], [], c='green', )
kinect_scatter = kinect_fig.scatter([], [], [], c='red')

# depth for mediapipe
depth = np.empty(33)
depth.fill(0.5)

# Total frames of sensors
# print("total mediapipe frame:",len(mediaDF))
# print("total radar frame:",len(range(radar_data.loc[0,"rdr_frame_no"],radar_data.loc[radar_data.index[-1],"rdr_frame_no"])))
# print("total kinect frame",len(kinect_data))

x_radar = []
y_radar = []
z_radar = []

# plot function


def update(i):

    # radar data normalisation
    # x_radar = (x_radar + 11)/22
    # y_radar = (y_radar + 11)/22
    # z_radar = (z_radar +11) /22

    # plotting point
    if (not (mediaDF.empty)):
        media_text.set_text(str(mediaTimeStamps[int(i//mediapipeRatio)]))
        media_scatter._offsets3d = (depth, x_points[int(
            i//mediapipeRatio)], (np.negative(y_points[int(i//mediapipeRatio)]))+1)
        # print("i:" , i, " raito:" ,int(i//mediapipeRatio)," point:", str(mediaTimeStamps[int(i//mediapipeRatio)]))

    if (not (radar_data.empty)):

        label = radar_data[(radar_data['rdr_frame_no'] == int(
            i//radarRatio) + firstFrame)].reset_index()
        radar_text.set_text(str(label.loc[0, "rdr_timestamp"]))
        x_radar = label['rdr_x_radar'].to_numpy()
        y_radar = label['rdr_y_radar'].to_numpy()
        z_radar = label['rdr_z_radar'].to_numpy()
        radar_scatter._offsets3d = (np.negative(z_radar), x_radar, y_radar)

    if (not (kinect_data.empty)):
        kinect_scatter._offsets3d = (kinect_z_points[int(i//kinectRatio)],
                                     kinect_y_points[int(i//kinectRatio)],
                                     kinect_x_points[int(i//kinectRatio)])
        kinect_text.set_text(str(kinect_timestamp[int(i//kinectRatio)]))


# interval variable
x = (duration / 30)*1000

# mediapipe plot settings
media_text = media_fig.text2D(
    0.5, -0.5, "2D Text", transform=media_fig.transAxes, ha="center")
media_fig.set_title("MediaPipe")
media_fig.set_xlabel('x-axis')
media_fig.set_ylabel('y-axis')
media_fig.set_zlabel('z-axis')
media_fig.set_xlim(0, 1)
media_fig.set_ylim(0, 1)
media_fig.set_zlim(0, 1)


# radar plot settings
radar_text = radar_fig.text2D(
    0.5, -0.5, "2D Text", transform=radar_fig.transAxes, ha="center")
radar_fig.set_title("Radar")
radar_fig.set_ylabel('y-axis')
radar_fig.set_zlabel('z-axis')
radar_fig.set_xlabel('x-axis')
radar_fig.set_xlim(-11, 11)
radar_fig.set_ylim(-11, 11)
radar_fig.set_zlim(-11, 11)

# kinect plot settings
kinect_text = kinect_fig.text2D(
    0.5, -0.5, "2D Text", transform=kinect_fig.transAxes, ha="center")
kinect_fig.set_title("Kinect")
kinect_fig.set_ylabel('y-axis')
kinect_fig.set_zlabel('z-axis')
kinect_fig.set_xlabel('x-axis')
kinect_fig.set_xlim(0, 10)
kinect_fig.set_ylim(0, 1)
kinect_fig.set_zlim(0, 1)

# animation function
ani = matplotlib.animation.FuncAnimation(
    fig, update, frames=range(0, int(duration*30)), interval=1, repeat=True)
plt.show()

exit(1)
# Saving as gif
gif = f"{viewer_path}/deney_{str(exp)}.gif"
writergif = animation.PillowWriter(fps=30)

ani.save(gif, writer=writergif)
print("GIF SAVED")
